<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
//serialización
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
//Filtros
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;


use App\Repository\PetRepository;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *    normalizationContext={
 *      "groups"={"pet:read"}
 *    },
 *    denormalizationContext={
 *      "groups"={"pet:write"}
 *    },
 * )
 * @ORM\Entity(repositoryClass=PetRepository::class)
 * @ApiFilter(DateFilter::class, properties={"birthdate"})
 * @ApiFilter(SearchFilter::class, properties={"specie":"exact","name":"partial","owner":"exact","owner.name":"partial"})
 * @ApiFilter(OrderFilter::class, properties={"id": "ASC", "name": "DESC"})
 * @ApiFilter(PropertyFilter::class, arguments={"parameterName": "properties", "overrideDefaultProperties": true, "whitelist": null})
 * @ApiFilter(GroupFilter::class, arguments={"parameterName": "groups", "overrideDefaultGroups": true, "whitelist": null})
 */
class Pet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"pet:write","pet:read","owner:custom:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"pet:write","pet:read"})
     */
    private $specie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"pet:write","pet:read","owner:custom:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"pet:write","pet:read","owner:custom:read"})
     */
    private $birthdate;

    /**
     * @ORM\ManyToOne(targetEntity=Owner::class, inversedBy="pets")
     * @Groups({"pet:write","pet:read"})
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity=Prescription::class, mappedBy="pet")
     * @Groups({"pet:write","pet:read","owner:custom:read"})
     * @ApiSubresource
     */
    private $prescriptions;

    public function __construct()
    {
        $this->prescriptions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpecie(): ?string
    {
        return $this->specie;
    }

    public function setSpecie(string $specie): self
    {
        $this->specie = $specie;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * @Groups({"pet:read","owner:custom:read"})
     * @SerializedName("age")
     */
    public function getAge():int{
        if ($this->birthdate){
            $interval = date_diff($this->birthdate,new \DateTime('now'));
            return $interval->y;
        }
        return 0;
    }

    public function getOwner(): ?Owner
    {
        return $this->owner;
    }

    public function setOwner(?Owner $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Prescription[]
     */
    public function getPrescriptions(): Collection
    {
        return $this->prescriptions;
    }

    public function addPrescription(Prescription $prescription): self
    {
        if (!$this->prescriptions->contains($prescription)) {
            $this->prescriptions[] = $prescription;
            $prescription->setPet($this);
        }

        return $this;
    }

    public function removePrescription(Prescription $prescription): self
    {
        if ($this->prescriptions->contains($prescription)) {
            $this->prescriptions->removeElement($prescription);
            // set the owning side to null (unless already changed)
            if ($prescription->getPet() === $this) {
                $prescription->setPet(null);
            }
        }

        return $this;
    }
}
