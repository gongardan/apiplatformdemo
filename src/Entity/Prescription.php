<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PrescriptionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *    subresourceOperations={
 *    "api_pets_prescriptions_get_subresource"={
 *       "method"="GET",
 *         "normalization_context"={"groups"={"prescription:detail:read"}}
 *      },
 *    },
 *    normalizationContext={
 *      "groups"={"prescription:read"}
 *    },
 *    denormalizationContext={
 *      "groups"={"prescription:write"}
 *    },
 * )
 * @ORM\Entity(repositoryClass=PrescriptionRepository::class)
 */
class Prescription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"prescription:write","prescription:read","prescription:detail:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"prescription:write","prescription:read","prescription:detail:read","pet:read"})
     */
    private $startdate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"prescription:write","prescription:read","prescription:detail:read"})
     */
    private $enddate;

    /**
     * @ORM\ManyToOne(targetEntity=Pet::class, inversedBy="prescriptions")
     * @Groups({"prescription:write","prescription:read","prescription:detail:read"})
     */
    private $pet;

    /**
     * @ORM\ManyToMany(targetEntity=Medicine::class, inversedBy="prescriptions")
     * @Groups({"prescription:write","prescription:read","prescription:detail:read"})
     */
    private $medicines;

    public function __construct()
    {
        $this->medicines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartdate(): ?\DateTimeInterface
    {
        return $this->startdate;
    }

    public function setStartdate(?\DateTimeInterface $startdate): self
    {
        $this->startdate = $startdate;

        return $this;
    }

    public function getEnddate(): ?\DateTimeInterface
    {
        return $this->enddate;
    }

    public function setEnddate(?\DateTimeInterface $enddate): self
    {
        $this->enddate = $enddate;

        return $this;
    }

    public function getPet(): ?Pet
    {
        return $this->pet;
    }

    public function setPet(?Pet $pet): self
    {
        $this->pet = $pet;

        return $this;
    }

    /**
     * @return Collection|Medicine[]
     */
    public function getMedicines(): Collection
    {
        return $this->medicines;
    }

    public function addMedicine(Medicine $medicine): self
    {
        if (!$this->medicines->contains($medicine)) {
            $this->medicines[] = $medicine;
        }

        return $this;
    }

    public function removeMedicine(Medicine $medicine): self
    {
        if ($this->medicines->contains($medicine)) {
            $this->medicines->removeElement($medicine);
        }

        return $this;
    }
}
